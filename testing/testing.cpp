﻿// import
#include <iostream>
#include <Windows.h>
#include <TlHelp32.h>

// nameSpace
using namespace std;


// logic ----------------------------------------------------
HWND getWin (LPCSTR const name) {
    HWND window = FindWindowA(name, NULL);
    return window;
}

void logger () {
    cout << "Finding Win" << '\n';
}


// entryPoint ----------------------------------------------------
int main() {
    HWND hwnd = getWin("Shell_TrayWnd");

    logger();
    ShowWindow(hwnd, SW_HIDE);
    Sleep(5000);
    ShowWindow(hwnd, SW_SHOW);

    return 0;
}
